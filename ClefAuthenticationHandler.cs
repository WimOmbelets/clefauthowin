﻿using Microsoft.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Owin.Security.Providers.Clef.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Owin.Security.Providers.Clef
{
    public class ClefAuthenticationHandler : AuthenticationHandler<ClefAuthenticationOptions>
    {
        private const string XmlSchemaString = "http://www.w3.org/2001/XMLSchema#string";
        private readonly ILogger logger;
        private readonly HttpClient httpClient;

        public ClefAuthenticationHandler(HttpClient httpClient, ILogger logger)
        {
            this.httpClient = httpClient;
            this.logger = logger;
        }

        protected override async Task<AuthenticationTicket> AuthenticateCoreAsync()
        {
            AuthenticationProperties properties = null;

            try
            {
                string code = null;

                IReadableStringCollection query = Request.Query;
                IList<string> values = query.GetValues("code");

                if(values != null && values.Count == 1)
                {
                    code = values[0];
                }

                if(!ValidateCorrelationId(properties, logger))
                {
                    return new AuthenticationTicket(null, properties);
                }

                string requestPrefix = Request.Scheme + "://" + Request.Host;
                string redirectUri = requestPrefix + Request.PathBase + Options.CallbackPath;

                // Build up the body for the token request
                var body = new List<KeyValuePair<string, string>>();
                //body.Add(new KeyValuePair<string, string>("grant_type", "authorization_code"));
                body.Add(new KeyValuePair<string, string>("code", code));
                //body.Add(new KeyValuePair<string, string>("redirect_uri", redirectUri));
                body.Add(new KeyValuePair<string, string>("app_id", Options.AppId));
                body.Add(new KeyValuePair<string, string>("app_secret", Options.AppSecret));
                
                string endPoint =
                    Options.AuthorizationEndPoint +
                    "?app_id=" + Uri.EscapeDataString(Options.AppId) +
                    "&app_secret=" + Uri.EscapeDataString(Options.AppSecret) +
                    "&code=" + Uri.EscapeDataString(code);

                // Request the token
                var requestMessage = new HttpRequestMessage(HttpMethod.Post, Options.AuthorizationEndPoint);
                requestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                requestMessage.Content = new FormUrlEncodedContent(body);
                HttpResponseMessage tokenResponse = await httpClient.SendAsync(requestMessage);
                tokenResponse.EnsureSuccessStatusCode();
                string text = await tokenResponse.Content.ReadAsStringAsync();

                //Deserialize token response
                dynamic response = JsonConvert.DeserializeObject<dynamic>(text);
                string accessToken = (string)response.access_token.token;
                string expires = (string)response.expires_in;
                string refreshToken = null;
                if(response.refresh_token != null)
                {
                    refreshToken = (string)response.refresh_token;
                }

                //Get the Clef user
                HttpResponseMessage graphResponse = await httpClient.GetAsync(
                    Options.InfoEndPoint + "?access_token=" + Uri.EscapeDataString(accessToken),
                    Request.CallCancelled
                );
                graphResponse.EnsureSuccessStatusCode();
                text = await graphResponse.Content.ReadAsStringAsync();
                JObject user = JObject.Parse(text);

                var context = new ClefAuthenticatedContext(Context, user, accessToken, expires, refreshToken);
                context.Identity = new ClaimsIdentity(
                    Options.AuthenticationType,
                    ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

                if(!String.IsNullOrEmpty(context.Id))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, context.Id, XmlSchemaString, Options.AuthenticationType));
                }

                if (!String.IsNullOrEmpty(context.FirstName))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.Name, context.FirstName, XmlSchemaString, Options.AuthenticationType));
                }

                if (!String.IsNullOrEmpty(context.LastName))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.Name, context.LastName, XmlSchemaString, Options.AuthenticationType));
                }

                if (!String.IsNullOrEmpty(context.Email))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.Email, context.Id, XmlSchemaString, Options.AuthenticationType));
                }

                context.Properties = properties;

                await Options.Provider.Authenticated(context);

                return new AuthenticationTicket(context.Identity, context.Properties);
            }
            catch (Exception ex)
            {
                logger.WriteError(ex.Message);
            }

            return new AuthenticationTicket(null, properties);
        }

        protected override Task ApplyResponseChallengeAsync()
        {
            if(Response.StatusCode != 401)
            {
                return Task.FromResult<object>(null);
            }

            AuthenticationResponseChallenge challenge = Helper.LookupChallenge(Options.AuthenticationType, Options.AuthenticationMode);

            if (challenge != null)
            {
                string baseUri =
                    Request.Scheme +
                    Uri.SchemeDelimiter +
                    Request.Host +
                    Request.PathBase;

                string currentUri =
                    baseUri +
                    Request.Path +
                    Request.QueryString;

                string redirectUri =
                    baseUri +
                    Options.CallbackPath;

                AuthenticationProperties properties = challenge.Properties;
                if (string.IsNullOrEmpty(properties.RedirectUri))
                {
                    properties.RedirectUri = currentUri;
                }

                // OAuth2 10.12 CSRF
                GenerateCorrelationId(properties);

                // comma separated
                string authorizationEndpoint =
                    Options.AuthorizationEndPoint +
                    "?response_type=code" +
                    "&app_id=" + Uri.EscapeDataString(Options.AppId) +
                    "&app_secret=" + Uri.EscapeDataString(Options.AppSecret);

                Response.Redirect(authorizationEndpoint);
            }

            return Task.FromResult<object>(null);

            //if (Response.StatusCode == 401)
            //{
            //    var challenge = Helper.LookupChallenge(Options.AuthenticationType, Options.AuthenticationMode);

            //    // Only react to 401 if there is an authentication challenge for the authentication
            //    // type of this handler.
            //    if (challenge != null)
            //    {
            //        var state = challenge.Properties;

            //        if (string.IsNullOrEmpty(state.RedirectUri))
            //        {
            //            state.RedirectUri = Request.Uri.ToString();
            //        }

            //        var stateString = Options.StateDataFormat.Protect(state);

            //        Response.Redirect(WebUtilities.AddQueryString(Options.CallbackPath.Value, "state", stateString));
            //    }
            //}

            //return Task.FromResult<object>(null);
        }

        public override async Task<bool> InvokeAsync()
        {
            // This is always invoked on each request. For passive middleware, only do anything if this is
            // for our callback path when the user is redirected back from the authentication provider.
            if (Options.CallbackPath.HasValue && Options.CallbackPath == Request.Path)
            {
                var ticket = await AuthenticateAsync();

                if (ticket != null)
                {
                    Context.Authentication.SignIn(ticket.Properties, ticket.Identity);

                    Response.Redirect(ticket.Properties.RedirectUri);

                    // Prevent further processing by the owin pipeline.
                    return true;
                }
            }
            // Let the rest of the pipeline run.
            return false;
        }
    }
}
