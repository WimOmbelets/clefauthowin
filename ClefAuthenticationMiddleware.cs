﻿﻿using Microsoft.Owin;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Owin.Security.Providers.Clef.Provider;
using System;
using System.Net.Http;

namespace Owin.Security.Providers.Clef
{
    public class ClefAuthenticationMiddleware : AuthenticationMiddleware<ClefAuthenticationOptions>
    {
        private readonly HttpClient httpClient;
        private readonly ILogger logger;

        public ClefAuthenticationMiddleware(OwinMiddleware next, IAppBuilder app, ClefAuthenticationOptions options)
            : base(next, options)
        {
            if(String.IsNullOrEmpty(Options.AppId))
            {
                throw new ArgumentException("AppId");
            }

            if(String.IsNullOrEmpty(Options.AppSecret))
            {
                throw new ArgumentException("AppSecret");
            }

            logger = app.CreateLogger<ClefAuthenticationMiddleware>();

            if(options.Provider == null)
            {
                Options.Provider = new ClefAuthenticationProvider();
            }

            if(String.IsNullOrEmpty(Options.SignInAsAuthenticationType))
            {
                Options.SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType();
            }

            httpClient = new HttpClient();
        }

        protected override AuthenticationHandler<ClefAuthenticationOptions> CreateHandler()
        {
            return new ClefAuthenticationHandler(httpClient, logger);
        }
    }
}
