﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Owin.Security.Providers.Clef.Provider;

namespace Owin.Security.Providers.Clef
{
    public class ClefAuthenticationOptions : AuthenticationOptions
    {
        public readonly string AuthorizationEndPoint = "https://clef.io/api/v1/authorize";
        public readonly string InfoEndPoint = "https://clef.io/api/v1/info";
        public readonly string LogoutEndPoint = "https://clef.io/api/v1/logout";

        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public PathString CallbackPath { get; set; }
        public IClefAuthenticationProvider Provider { get; set; }
        public string SignInAsAuthenticationType { get; set; }

        public ClefAuthenticationOptions(string appId, string appSecret, string callbackPath)
            : base("Clef")
        {
            if (String.IsNullOrEmpty(appId))
            {
                throw new ArgumentException("appId");
            }

            if (String.IsNullOrEmpty(appSecret))
            {
                throw new ArgumentException("appSecret");
            }

            if (String.IsNullOrEmpty(callbackPath))
            {
                throw new ArgumentException("callbackPath");
            }

            Description.Caption = Constants.DefaultAuthenticationType;
            AppId = appId;
            AppSecret = appSecret;
            CallbackPath = new PathString(callbackPath);
            AuthenticationMode = AuthenticationMode.Passive;
        }
    }
}
