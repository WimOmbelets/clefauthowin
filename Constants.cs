﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Owin.Security.Providers.Clef
{
    internal static class Constants
    {
        internal const string DefaultAuthenticationType = "Clef";
    }
}
