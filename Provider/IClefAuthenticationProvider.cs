﻿using System.Threading.Tasks;

namespace Owin.Security.Providers.Clef.Provider
{
    public interface IClefAuthenticationProvider
    {
        Task Authenticated(ClefAuthenticatedContext context);
        Task ReturnEndPoint(ClefReturnEndPointContext context);
    }
}
