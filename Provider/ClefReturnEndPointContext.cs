﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;

namespace Owin.Security.Providers.Clef.Provider
{
    public class ClefReturnEndPointContext : ReturnEndpointContext
    {
        public ClefReturnEndPointContext(IOwinContext context, AuthenticationTicket ticket)
            : base(context, ticket)
        {

        }
    }
}
