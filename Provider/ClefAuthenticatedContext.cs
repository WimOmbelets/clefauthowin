﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;
using Newtonsoft.Json.Linq;

namespace Owin.Security.Providers.Clef.Provider
{
    public class ClefAuthenticatedContext : BaseContext
    {
        public JObject User { get; private set; }
        public string AccessToken { get; private set; }
        public string RefreshToken { get; private set; }
        public TimeSpan? ExpiresIn { get; private set; }
        public string Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; set; }
        public string Email { get; private set; }
        public ClaimsIdentity Identity { get; set; }
        public AuthenticationProperties Properties { get; set; }

        private static string TryGetValue(JObject user, string propertyName)
        {
            JToken value;
            return user.TryGetValue(propertyName, out value) ? value.ToString() : null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClefAuthenticatedContext"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The JSON-serialized user.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="expires">The expires.</param>
        /// <param name="refreshToken">The refresh token.</param>
        public ClefAuthenticatedContext(IOwinContext context, JObject user, string accessToken, 
            string expires, string refreshToken)
            : base(context)
        {
            User = user;
            AccessToken = accessToken;
            RefreshToken = refreshToken;

            int expiresValue;
            if (Int32.TryParse(expires, NumberStyles.Integer, CultureInfo.InvariantCulture, out expiresValue))
            {
                ExpiresIn = TimeSpan.FromSeconds(expiresValue);
            }

            Id = TryGetValue(user, "id");
            FirstName = TryGetValue(user, "first_name");
            LastName = TryGetValue(user, "last_name");
            Email = TryGetValue(user, "email");
        }
    }
}
