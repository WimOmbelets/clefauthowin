﻿using System;
using System.Threading.Tasks;

namespace Owin.Security.Providers.Clef.Provider
{
    class ClefAuthenticationProvider : IClefAuthenticationProvider
    {
        public Func<ClefAuthenticatedContext, Task> OnAuthenticated { get; set; }
        public Func<ClefReturnEndPointContext, Task> OnReturnEndPoint { get; set; }


        public ClefAuthenticationProvider()
        {
            OnAuthenticated = context => Task.FromResult<object>(null);
            OnReturnEndPoint = context => Task.FromResult<object>(null);
        }

        public virtual Task Authenticated(ClefAuthenticatedContext context)
        {
            return OnAuthenticated(context);
        }

        public virtual Task ReturnEndPoint(ClefReturnEndPointContext context)
        {
            return OnReturnEndPoint(context);
        }
    }
}
