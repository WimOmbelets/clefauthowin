﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Owin.Security.Providers.Clef
{
    public static class ClefAuthenticationExtensions
    {
        public static IAppBuilder UseClefAuthentication(this IAppBuilder app, ClefAuthenticationOptions options)
        {
            if(app == null)
            {
                throw new ArgumentException("app");
            }

            if(options == null)
            {
                throw new ArgumentException("options");
            }

            return app.Use(typeof(ClefAuthenticationMiddleware), app, options);
        }

        public static IAppBuilder UseClefAuthentication(this IAppBuilder app, string appId, string appSecret, string callbackPath)
        {
            return app.UseClefAuthentication(new ClefAuthenticationOptions(appId, appSecret, callbackPath)
            {
                AppId = appId,
                AppSecret = appSecret,
                CallbackPath = new PathString(callbackPath)
            });
        }
    }
}
